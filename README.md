# CARTEIRINHA

## INSTALL DEPENDENCIES

```
npm install
```

## RUNNING LOCAL SERVER

### Start mongodb
```
mongod --dbpath=/db/data
```

### Start node SERVER
```
npm start
```

## TODO

- [ ] Unit testing
- [ ] API testing
- [ ] Lint (checkstyle)
- [ ] Istanbul (coverage)
